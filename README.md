# Android Vulkan mprotect

This example tests the feasibility of a Vulkan implementation to support protecting `VkMemory` via
the POSIX [mprotect](https://man7.org/linux/man-pages/man2/mprotect.2.html) function.

The motivation is test compatibility for [ANGLE's capture functionality](https://source.chromium.org/chromium/chromium/src/+/main:third_party/angle/src/libANGLE/capture/FrameCapture.h;l=523;drc=72186360334350c90b9b94515b3c82bea25e27ff)
for the `OpenGL ES 3.1` [EXT_buffer_storage](https://www.khronos.org/registry/OpenGL/extensions/EXT/EXT_buffer_storage.txt) extension.

## Android app

This repository consists of an Android application that enumerates all feasible `VkMemoryType` and runs several `mprotect` tests. The C++ implementation of this can be found in `app/src/main/cpp/main.cpp`.

## Minimal test

Furthermore it contains a minimal Android test, which is implemented in `app/src/main/cpp/minimal.cpp`, which only tried to initialize a preferred `VkMemory` and tries to call `mprotect` on it.

## VkMemoryPropertyFlags

When allocating buffers for `glBufferStorageEXT` ANGLE's Vulkan backend requests the following `VkMemoryPropertyFlags`:

### Required
`VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT`
### Preferred
`VK_MEMORY_PROPERTY_HOST_COHERENT_BIT` and `VK_MEMORY_PROPERTY_HOST_CACHED_BIT`

As seen in [BufferVk.cpp](https://source.chromium.org/chromium/chromium/src/+/main:third_party/angle/src/libANGLE/renderer/vulkan/BufferVk.cpp;l=313;drc=72186360334350c90b9b94515b3c82bea25e27ff)

These flags checked for and printed in the test results, which can be seen in `logcat`.

## Compatibility

The project was tested on *Android 11* and *Android 12* and has a minimal Android API requirement of `API 30` and
targets SDK `API 32`, which is the latest at the time of writing.
It can probably be easily back ported to older SDK versions if required.
The C++ code is portable to other POSIX platforms.