// Copyright 2018 The ANGLE Project Authors. All rights reserved.
// Copyright 2022 Google Inc. All Rights Reserved.
// Copyright 2022 Collabora Ltd.
// Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
// SPDX-License-Identifier: Apache-2.0

// Based on the Android Vulkan Tutorials
// https://github.com/googlesamples/android-vulkan-tutorials

// Based on ANGLE's mprotect implementation
// https://chromium.googlesource.com/angle/angle.git/+/refs/heads/main/src/common/system_utils_posix.cpp

// Minimal test that only allocates the most preferred vulkan memory type and tries to protect it.

#include <sys/mman.h>
#include <unistd.h>

#include <vector>

#include <android/log.h>
#include <vulkan_wrapper.h>
#include <jni.h>

#include "round.h"

// Android log function wrappers
static const char *kAppName = "vulkan-mprotect-minimal";
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, kAppName, __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, kAppName, __VA_ARGS__))

#define FAIL_IF(condition, message)                  \
  if (condition) {                                   \
    LOGE("%s (%s:%d)", message, __FILE__, __LINE__); \
    return false;                                    \
  }

bool sortByPoints(const std::pair<uint32_t, uint32_t> &a, const std::pair<uint32_t, uint32_t> &b) {
    return a.second > b.second;
}

// Just an empty handler
void SegfaultHandlerFunction(int sig, siginfo_t *info, void *unused) {
    (void) sig;
    (void) info;
    (void) unused;
}

extern "C" JNIEXPORT jboolean JNICALL
Java_com_example_vulkanmprotect_MinimalTest_runMinimalMprotect(
        JNIEnv *env,
        jobject /* this */) {

    (void) env;

    FAIL_IF(!InitVulkan(), "Could not initialize Vulkan.")

    VkInstance instance;
    VkDevice device;
    VkPhysicalDevice gpu;

    // Init Vk instance
    VkApplicationInfo appInfo{
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pApplicationName = kAppName,
            .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
            .pEngineName = kAppName,
            .engineVersion = VK_MAKE_VERSION(1, 0, 0),
            .apiVersion = VK_MAKE_VERSION(1, 1, 0),
    };

    VkInstanceCreateInfo instanceCreateInfo{
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pApplicationInfo = &appInfo,
    };

    VkResult res = vkCreateInstance(&instanceCreateInfo, nullptr, &instance);
    FAIL_IF(res != VK_SUCCESS, "Could not initialize Vulkan instance.")

    // Init Vk device
    uint32_t gpuCount = 0;
    res = vkEnumeratePhysicalDevices(instance, &gpuCount, nullptr);
    FAIL_IF(res != VK_SUCCESS, "Could not enumerate physical devices.")
    VkPhysicalDevice tmpGpus[gpuCount];
    res = vkEnumeratePhysicalDevices(instance, &gpuCount, tmpGpus);
    FAIL_IF(res != VK_SUCCESS, "Could not enumerate physical devices.")
    gpu = tmpGpus[0];  // Pick up the first GPU Device

    // Find a GFX queue family
    uint32_t queueFamilyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, nullptr);
    FAIL_IF(queueFamilyCount == 0, "Could not enumerate queues.")
    std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount,
                                             queueFamilyProperties.data());

    uint32_t queueFamilyIndex;
    for (queueFamilyIndex = 0; queueFamilyIndex < queueFamilyCount;
         queueFamilyIndex++) {
        if (queueFamilyProperties[queueFamilyIndex].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            break;
        }
    }
    FAIL_IF(queueFamilyIndex >= queueFamilyCount, "Could not find graphics queue.")

    // Create a logical device from GPU we picked
    float priorities[] = {
            1.0f,
    };
    VkDeviceQueueCreateInfo queueCreateInfo{
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = queueFamilyIndex,
            .queueCount = 1,
            .pQueuePriorities = priorities,
    };

    VkDeviceCreateInfo deviceCreateInfo{
            .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
            .queueCreateInfoCount = 1,
            .pQueueCreateInfos = &queueCreateInfo,
    };
    res = vkCreateDevice(gpu, &deviceCreateInfo, nullptr, &device);

    FAIL_IF(res != VK_SUCCESS, "Could not init Vulkan device")

    // Get page size
    long pageSizeLong = sysconf(_SC_PAGE_SIZE);
    if (pageSizeLong < 0) {
        LOGE("Could not get sysconf page size. errno %d: '%s'", errno, strerror(errno));
        return false;
    }

    auto pageSize = static_cast<size_t>(pageSizeLong);

    // Enumerate memory types
    VkPhysicalDeviceMemoryProperties memoryProperties;
    vkGetPhysicalDeviceMemoryProperties(gpu, &memoryProperties);

    std::vector<std::pair<uint32_t, uint32_t>> memoryIndexPoints;

    for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++) {
        VkMemoryType memoryType = memoryProperties.memoryTypes[i];

        // Skip protected and lazily allocated
        if ((memoryType.propertyFlags & VK_MEMORY_PROPERTY_PROTECTED_BIT) ||
            (memoryType.propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT)) {
            continue;
        }

        // Only consider VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT as minimal requirement.
        if (memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
            uint32_t points = 0;

            // Prefer memory types with these flags
            if (memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) {
                points += 1;
            }
            if (memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) {
                points += 1;
            }

            memoryIndexPoints.emplace_back(std::pair<uint32_t, uint32_t>(i, points));
        }
    }

    FAIL_IF(memoryIndexPoints.empty(), "Could not find any viable memory type.")

    sort(memoryIndexPoints.begin(), memoryIndexPoints.end(), sortByPoints);

    uint32_t bestIndex = memoryIndexPoints[0].first;

    LOGI("Testing best memory index %u", bestIndex);

    // Allocate memory
    VkMemoryAllocateInfo allocateInfo{
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .allocationSize = pageSize,
            .memoryTypeIndex = bestIndex
    };

    VkDeviceMemory memory;
    res = vkAllocateMemory(device, &allocateInfo, nullptr, &memory);
    FAIL_IF(res != VK_SUCCESS, "Could not allocate vk memory")

    void *map;
    res = vkMapMemory(device, memory, 0, VK_WHOLE_SIZE, 0, &map);
    FAIL_IF(res != VK_SUCCESS, "Could not map vk memory")

    // Test mprotect
    auto start = reinterpret_cast<uintptr_t>(map);

    uintptr_t protectionStart = roundDownPow2(start, pageSize);
    uintptr_t protectionEnd = roundUpPow2(start + pageSize, pageSize);

    struct sigaction sigAction = {};
    sigAction.sa_flags = SA_SIGINFO;
    sigAction.sa_sigaction = &SegfaultHandlerFunction;
    sigemptyset(&sigAction.sa_mask);

    if (sigaction(SIGSEGV, &sigAction, nullptr) != 0) {
        LOGE("Could not set mprotect handler function: errno %d: '%s'", errno, strerror(errno));
        return false;
    }

    size_t protectionSize = protectionEnd - protectionStart;

    int ret = mprotect(reinterpret_cast<void *>(protectionStart), protectionSize, PROT_READ);
    if (ret < 0) {
        LOGE("mprotect failed with errno %d: '%s'", errno, strerror(errno));
        return false;
    }

    // Clean up
    vkUnmapMemory(device, memory);
    vkDestroyDevice(device, nullptr);
    vkDestroyInstance(instance, nullptr);

    return true;
}
