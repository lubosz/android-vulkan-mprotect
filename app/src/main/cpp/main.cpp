// Copyright 2018 The ANGLE Project Authors. All rights reserved.
// Copyright 2022 Google Inc. All Rights Reserved.
// Copyright 2022 Collabora Ltd.
// Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
// SPDX-License-Identifier: Apache-2.0

// Based on the Android Vulkan Tutorials
// https://github.com/googlesamples/android-vulkan-tutorials

// Based on ANGLE's mprotect implementation
// https://chromium.googlesource.com/angle/angle.git/+/refs/heads/main/src/common/system_utils_posix.cpp

// Comprehensive test that enumerates all memory types and runs mprotect tests on them.

#include <sys/mman.h>
#include <unistd.h>

#include <vector>
#include <mutex>
#include <sstream>
#include <functional>

#include <android/log.h>
#include <vulkan_wrapper.h>
#include <jni.h>

#include "round.h"

// Android log function wrappers
static const char *kAppName = "vulkan-mprotect";
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, kAppName, __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, kAppName, __VA_ARGS__))

// Vulkan call wrapper
#define CALL_VK(func)                                             \
  if (VK_SUCCESS != (func)) {                                     \
    LOGE("Vulkan error. File[%s], line[%d]", __FILE__, __LINE__); \
    assert(false);                                                \
  }

// Global variables
VkInstance instance;
VkDevice device;
VkPhysicalDevice gpu;

std::vector<jint> successfulIndices;

size_t getPageSize() {
    long pageSize = sysconf(_SC_PAGE_SIZE);
    if (pageSize < 0) {
        LOGE("Could not get sysconf page size. errno %d: '%s'", errno, strerror(errno));
        return 0;
    }
    return static_cast<size_t>(pageSize);
}

// Return type of the PageFaultCallback
enum class PageFaultHandlerRangeType {
    // The memory address was known by the page fault handler
    InRange,
    // The memory address was not in the page fault handler's range
    // and the signal will be forwarded to the default page handler.
    OutOfRange,
};

using PageFaultCallback = std::function<PageFaultHandlerRangeType(uintptr_t)>;

bool SetMemoryProtection(uintptr_t start, size_t size, int protections) {
    int ret = mprotect(reinterpret_cast<void *>(start), size, protections);
    if (ret < 0) {
        LOGE("mprotect failed with errno %d: '%s'", errno, strerror(errno));
    }
    return ret == 0;
}

class PageFaultHandler {
public:
    explicit PageFaultHandler(PageFaultCallback callback) : mCallback(std::move(callback)) {}
    ~PageFaultHandler() = default;
    bool enable();
    bool disable();
    void handle(int sig, siginfo_t *info, void *unused);

private:
    struct sigaction mDefaultSegvAction = {};
    PageFaultCallback mCallback;
};

PageFaultHandler *gPosixPageFaultHandler = nullptr;

void SegfaultHandlerFunction(int sig, siginfo_t *info, void *unused) {
    gPosixPageFaultHandler->handle(sig, info, unused);
}

void PageFaultHandler::handle(int sig, siginfo_t *info, void *unused) {
    bool found = false;
    // Android 12 uses SEGV_MAPERR, where SEGV_ACCERR is used on older versions.
    if (sig == SIGSEGV && (info->si_code == SEGV_ACCERR || info->si_code == SEGV_MAPERR)) {
        found = mCallback(reinterpret_cast<uintptr_t>(info->si_addr)) ==
                PageFaultHandlerRangeType::InRange;
    }

    // Fall back to default signal handler
    if (!found) {
        if (sig == SIGSEGV) {
            mDefaultSegvAction.sa_sigaction(sig, info, unused);
        } else {
            assert(false);
        }
    }
}

bool PageFaultHandler::disable() {
    return sigaction(SIGSEGV, &mDefaultSegvAction, nullptr) == 0;
}

bool PageFaultHandler::enable() {
    struct sigaction sigAction = {};
    sigAction.sa_flags = SA_SIGINFO;
    sigAction.sa_sigaction = &SegfaultHandlerFunction;
    sigemptyset(&sigAction.sa_mask);

    return sigaction(SIGSEGV, &sigAction, &mDefaultSegvAction) == 0;
}


// Set write protection
bool ProtectMemory(uintptr_t start, size_t size) {
    return SetMemoryProtection(start, size, PROT_READ);
}

// Allow reading and writing
bool UnprotectMemory(uintptr_t start, size_t size) {
    return SetMemoryProtection(start, size, PROT_READ | PROT_WRITE);
}

PageFaultHandler *CreatePageFaultHandler(PageFaultCallback callback) {
    gPosixPageFaultHandler = new PageFaultHandler(std::move(callback));
    return gPosixPageFaultHandler;
}

bool doTestMProtect(size_t size, uintptr_t start) {
    size_t pageSize = getPageSize();

    uintptr_t protectionStart = roundDownPow2(start, pageSize);

    LOGI("\t\tPage aligned: %d", protectionStart == start);

    uintptr_t protectionEnd = roundUpPow2(start + size, pageSize);

    std::mutex mutex;
    bool handlerCalled = false;

    PageFaultCallback callback = [&mutex, pageSize, start, size, &handlerCalled](
            uintptr_t address) {
        if (address >= start && address < start + size) {
            std::lock_guard<std::mutex> lock(mutex);
            uintptr_t pageStart = roundDownPow2(address, pageSize);
            assert(UnprotectMemory(pageStart, pageSize) == true);
            handlerCalled = true;
            return PageFaultHandlerRangeType::InRange;
        } else {
            return PageFaultHandlerRangeType::OutOfRange;
        }
    };

    std::unique_ptr<PageFaultHandler> handler(CreatePageFaultHandler(callback));

    handler->enable();

    size_t protectionSize = protectionEnd - protectionStart;

    // Test Protect
    if (!ProtectMemory(protectionStart, protectionSize)) {
        LOGE("Protecting memory failed.");
        return false;
    }

    // Write
    auto bytes = reinterpret_cast<uint8_t *>(start);
    bytes[0] = 0;

    // Wait and validate
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (!handlerCalled) {
            LOGE("Protection handler could not be called successfully.");
            return false;
        }
    }

    // Test Protect and unprotect
    if (!ProtectMemory(protectionStart, protectionSize)) {
        LOGE("Protecting memory failed.");
        return false;
    }
    if (!UnprotectMemory(protectionStart, protectionSize)) {
        LOGE("Unprotecting memory failed.");
        return false;
    }

    handlerCalled = false;
    bytes[0] = 0;
    if (handlerCalled) {
        LOGE("Protection was incorrectly called.");
        return false;
    }

    // Clean up
    if (!handler->disable()) {
        LOGE("Could not unregister protection handler.");
        return false;
    }

    LOGI("\t\tSuccess.");

    return true;
}

void testMProtectStack() {
    std::vector<float> data = std::vector<float>(100);
    size_t size = sizeof(float) * data.size();
    auto start = reinterpret_cast<uintptr_t>(data.data());
    doTestMProtect(size, start);
}

void testMProtectVkMemory(VkDeviceSize size, uint32_t typeIndex) {
    VkMemoryAllocateInfo allocateInfo{
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .pNext = nullptr,
            .allocationSize = size,
            .memoryTypeIndex = typeIndex
    };

    VkDeviceMemory memory;
    VkResult res = vkAllocateMemory(device, &allocateInfo, nullptr, &memory);
    CALL_VK(res)

    void *data;
    res = vkMapMemory(device, memory, 0, VK_WHOLE_SIZE, 0, &data);
    CALL_VK(res)

    if(doTestMProtect(size, reinterpret_cast<uintptr_t>(data))) {
        successfulIndices.push_back(static_cast<jint>(typeIndex));
    }

    vkUnmapMemory(device, memory);
}

#define CHECK_FLAG(f) if (flags & (f)) result << #f << " "

std::string heapFlagsStr(VkMemoryHeapFlags flags) {
    std::stringstream result;
    CHECK_FLAG(VK_MEMORY_HEAP_DEVICE_LOCAL_BIT);
    CHECK_FLAG(VK_MEMORY_HEAP_MULTI_INSTANCE_BIT);
    return result.str();
}

std::string propertyFlagsStr(VkMemoryPropertyFlags flags) {
    std::stringstream result;
    CHECK_FLAG(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    CHECK_FLAG(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
    CHECK_FLAG(VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    CHECK_FLAG(VK_MEMORY_PROPERTY_HOST_CACHED_BIT);
    CHECK_FLAG(VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT);
    CHECK_FLAG(VK_MEMORY_PROPERTY_PROTECTED_BIT);
    CHECK_FLAG(VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD);
    CHECK_FLAG(VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD);
    return result.str();
}

void logVersion(const char *label, uint32_t version) {
    LOGI("%s: %d.%d.%d",
         label,
         VK_VERSION_MAJOR(version),
         VK_VERSION_MINOR(version),
         VK_VERSION_PATCH(version));
}

void testAllMemoryTypes() {
    size_t pageSize = getPageSize();
    LOGI("page size: %lu\n", (unsigned long) pageSize);

    LOGI("Testing stack:");
    testMProtectStack();

    VkPhysicalDeviceMemoryProperties memoryProperties;
    vkGetPhysicalDeviceMemoryProperties(gpu, &memoryProperties);

    for (uint32_t i = 0; i < memoryProperties.memoryHeapCount; i++) {
        VkMemoryHeap heap = memoryProperties.memoryHeaps[i];
        LOGI("Heap #%d (%lu bytes): %s", i,
             static_cast<unsigned long>(heap.size), heapFlagsStr(heap.flags).c_str());
        for (uint32_t j = 0; j < memoryProperties.memoryTypeCount; j++) {
            VkMemoryType memoryType = memoryProperties.memoryTypes[j];
            if (memoryType.heapIndex == i) {
                LOGI("\tType #%d: %s", j, propertyFlagsStr(memoryType.propertyFlags).c_str());

                if (memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
                    LOGI("\t\tHas ANGLE required flag VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT.");
                }

                if (memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) {
                    LOGI("\t\tHas ANGLE preferred flag VK_MEMORY_PROPERTY_HOST_COHERENT_BIT.");
                }

                if (memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) {
                    LOGI("\t\tHas ANGLE preferred flag VK_MEMORY_PROPERTY_HOST_CACHED_BIT.");
                }

                // Don't test protected and lazily allocated
                if ((memoryType.propertyFlags & VK_MEMORY_PROPERTY_PROTECTED_BIT) ||
                    (memoryType.propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT)) {
                    LOGI("\t\tSkipping unsupported memory type.");
                } else {
                    testMProtectVkMemory(pageSize, j);
                }
            }
        }
    }
}

void initInstance() {
    VkApplicationInfo appInfo{
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pNext = nullptr,
            .pApplicationName = kAppName,
            .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
            .pEngineName = kAppName,
            .engineVersion = VK_MAKE_VERSION(1, 0, 0),
            .apiVersion = VK_MAKE_VERSION(1, 1, 0),
    };

    // Create the Vulkan instance
    VkInstanceCreateInfo instanceCreateInfo{
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pNext = nullptr,
            .pApplicationInfo = &appInfo,
            .enabledLayerCount = 0,
            .ppEnabledLayerNames = nullptr,
            .enabledExtensionCount = 0,
            .ppEnabledExtensionNames = nullptr,
    };
    CALL_VK(vkCreateInstance(&instanceCreateInfo, nullptr, &instance))
}

void initDevice() {
    // Find one GPU to use:
    // On Android, every GPU device is equal -- supporting
    // graphics/compute/present
    // for this sample, we use the very first GPU device found on the system
    uint32_t gpuCount = 0;
    CALL_VK(vkEnumeratePhysicalDevices(instance, &gpuCount, nullptr))
    VkPhysicalDevice tmpGpus[gpuCount];
    CALL_VK(vkEnumeratePhysicalDevices(instance, &gpuCount, tmpGpus))
    gpu = tmpGpus[0];  // Pick up the first GPU Device

    // check for vulkan info on this GPU device
    VkPhysicalDeviceProperties gpuProperties;
    vkGetPhysicalDeviceProperties(gpu, &gpuProperties);
    LOGI("GPU: %s", gpuProperties.deviceName);
    LOGI("vendorID: 0x%x", gpuProperties.vendorID);
    LOGI("deviceID: 0x%x", gpuProperties.deviceID);
    logVersion("driverVersion", gpuProperties.driverVersion);
    logVersion("apiVersion", gpuProperties.apiVersion);

    // Find a GFX queue family
    uint32_t queueFamilyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, nullptr);
    assert(queueFamilyCount);
    std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount,
                                             queueFamilyProperties.data());

    uint32_t queueFamilyIndex;
    for (queueFamilyIndex = 0; queueFamilyIndex < queueFamilyCount;
         queueFamilyIndex++) {
        if (queueFamilyProperties[queueFamilyIndex].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            break;
        }
    }
    assert(queueFamilyIndex < queueFamilyCount);

    // Create a logical device from GPU we picked
    float priorities[] = {
            1.0f,
    };
    VkDeviceQueueCreateInfo queueCreateInfo{
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .queueFamilyIndex = queueFamilyIndex,
            .queueCount = 1,
            .pQueuePriorities = priorities,
    };

    VkDeviceCreateInfo deviceCreateInfo{
            .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
            .pNext = nullptr,
            .queueCreateInfoCount = 1,
            .pQueueCreateInfos = &queueCreateInfo,
            .enabledLayerCount = 0,
            .ppEnabledLayerNames = nullptr,
            .enabledExtensionCount = 0,
            .ppEnabledExtensionNames = nullptr,
            .pEnabledFeatures = nullptr,
    };

    CALL_VK(vkCreateDevice(gpu, &deviceCreateInfo, nullptr, &device))
}

extern "C" JNIEXPORT jintArray JNICALL
Java_com_example_vulkanmprotect_MainActivity_runAllMProtectTests(
        JNIEnv *env,
        jobject /* this */) {

    if (!InitVulkan()) {
        LOGE("Vulkan is unavailable, install vulkan and re-start");
        return nullptr;
    }

    initInstance();
    initDevice();

    testAllMemoryTypes();

    vkDestroyDevice(device, nullptr);
    vkDestroyInstance(instance, nullptr);

    jintArray result = env->NewIntArray(static_cast<jsize>(successfulIndices.size()));
    env->SetIntArrayRegion(result, 0, static_cast<jsize>(successfulIndices.size()),
                           successfulIndices.data());

    return result;
}
