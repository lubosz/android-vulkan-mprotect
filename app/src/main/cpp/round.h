// Copyright 2002 The ANGLE Project Authors. All rights reserved.
// Based on ANGLE's mathutil.h
// https://chromium.googlesource.com/angle/angle.git/+/refs/heads/main/LICENSE

#pragma once

#include <cassert>

template<typename T>
inline constexpr bool isPow2(T x) {
    static_assert(std::is_integral<T>::value, "isPow2 must be called on an integer type.");
    return (x & (x - 1)) == 0 && (x != 0);
}

template<typename T>
constexpr T roundUpPow2(const T value, const T alignment) {
    assert(isPow2(alignment));
    return (value + alignment - 1) & ~(alignment - 1);
}

template<typename T>
constexpr T roundDownPow2(const T value, const T alignment) {
    assert(isPow2(alignment));
    return value & ~(alignment - 1);
}